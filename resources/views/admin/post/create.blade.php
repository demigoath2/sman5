@extends('backend.index')
@section('sub-judul','Tambah Post')
@section('halaman-sekarang','Post')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('post.index') }}" class="btn btn-warning btn-sm">Kembali</a>
        </div>
      </div>
      <div class="card-body">

        <div class="row justify-content-center">
          <div class="col-md-8">
            <form class="form-horizontal" method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data">
              @csrf

              <div class="form-group">
                <label>Judul</label>
                <input type="text" class="form-control" name="judul" placeholder="Input Judul">
                <div class="text-danger">@error('judul') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Kategori</label>
                <select class="form-control" name="category_id">
                  <option value="" holder>Pilih Kategori</option>
                  @foreach ($category as $result)
                  <option value="{{$result->id }}">{{ $result->name}}</option>
                  @endforeach
                </select>
                <div class="text-danger">@error('category_id') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Pilih Tags</label>
                <select class="form-control" name="tags">
                  <option value="">Pilih Tags</option>
                  @foreach($tags as $tag)
                  <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                  @endforeach
                </select>
                <div class="text-danger">@error('tags') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Thumbnail</label>
                <input type="file" class="form-control" name="foto">
                <div class="text-danger">@error('foto') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Konten</label>
                <textarea class="form-control" name="content"></textarea>
                <div class="text-danger">@error('content') {{ $message }} @enderror</div>
              </div>
              <button type="submit" class="btn btn-info">Simpan</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script >
  CKEDITOR.replace( 'content' );

</script>

@endsection
