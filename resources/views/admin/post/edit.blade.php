@extends('backend.index')
@section('sub-judul','Edit Post')
@section('halaman-sekarang','Edit Post')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('post.index') }}" class="btn btn-warning btn-sm">Kembali</a>
        </div>
      </div>
      <div class="card-body">

        <div class="row justify-content-center">
          <div class="col-md-8">
            <form class="form-horizontal" method="POST" action="{{ route('post.update', $post->id ) }}" enctype="multipart/form-data">
              @csrf
              @method('PUT')

              <div class="form-group">
                <label>Judul</label>
                <input type="text" class="form-control" name="judul" value="{{ $post->judul }}">
                <div class="text-danger">@error('judul') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Kategori</label>
                <select class="form-control" name="category_id">
                  <option value="" holder>Pilih Kategori</option>
                  @foreach ($category as $result)
                  <option value="{{$result->id }}" {{ $result->id == $post->category_id ? 'selected' : ''}}>{{ $result->name}}</option>
                  @endforeach
                </select>
                <div class="text-danger">@error('category_id') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Thumbnail</label>
                <input type="file" class="form-control" name="foto">
                <div class="text-danger">@error('foto') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Konten</label>
                <textarea class="form-control" name="content">{{ $post->content }}</textarea>
                <div class="text-danger">@error('content') {{ $message }} @enderror</div>
              </div>
              <button type="submit" class="btn btn-info">Simpan</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script >
  CKEDITOR.replace( 'content' );

</script>


@endsection
