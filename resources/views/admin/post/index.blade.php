@extends('backend.index')
@section('sub-judul','Post')
@section('halaman-sekarang','Post')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('post.create') }}" class="btn btn-primary btn-flat btn-sm"><i class="fas fa-plus"></i> Tambah</a>
        </div>
      </div>

      <div class="card-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th width="10%">No</th>
              <th>Judul Post</th>
              <th>Kategori</th>
              <th>Foto</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach( $post as $result => $hasil)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $hasil->judul }}</td>
              <td>{{ $hasil->category->name}}</td>
              <td><img src="{{ asset( $hasil->foto ) }}" class="img-fluid" width="100px" alt=""></td>
              <td>
                <form action="{{ route('post.destroy', $hasil->id )}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="{{ route('post.edit', $hasil->id ) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                  <button type="submit" class="btn btn-danger btn-sm "><i class="fas fa-trash"></i></button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


@endsection
