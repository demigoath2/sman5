@extends('backend.index')
@section('sub-judul','Tambah Fasilitas')
@section('halaman-sekarang','Tambah Fasilitas')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('fasilitas.index') }}" class="btn btn-warning btn-sm">Kembali</a>
        </div>
      </div>
      <div class="card-body">

        <div class="row justify-content-center">
          <div class="col-md-8">
            <form class="form-horizontal" method="POST" action="{{ route('fasilitas.store') }}" enctype="multipart/form-data">
              @csrf

              <div class="form-group">
                <label>Nama Pengelola</label>
                <input type="text" class="form-control" name="name" placeholder="Input Nama">
                <div class="text-danger">@error('name') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Thumbnail</label>
                <input type="file" class="form-control" id="thumbnail" name="foto" accept=".jpg, .png, .jpeg">
                <div class="text-danger">@error('foto') {{ $message }} @enderror</div>
              </div>
              <div id="thumbnail-preview"></div>
              <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" name="content" id="content"></textarea>
                <div class="text-danger">@error('content') {{ $message }} @enderror</div>
              </div>
              <button type="submit" class="btn btn-info">Simpan</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    CKEDITOR.replace( 'content' );

    $("#thumbnail").on('change', function(){
      thumbnail(this);
    });
  })
  function thumbnail(input) {
    if (input.files && input.files[0]) {
      var thumnail_preview = $('#thumbnail-preview');
      thumnail_preview.empty();
      var reader = new FileReader();

      reader.onload = function(e) {
        $("<img />", {
          "src": e.target.result,
          "class": "preview-img pb-3",
          "width": "300",
          "height": "300"
        }).appendTo(thumnail_preview);
      }
      thumnail_preview.show();
      reader.readAsDataURL(input.files[0]);
    }
  }
</script>

@endsection
