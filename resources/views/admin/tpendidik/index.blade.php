@extends('backend.index')
@section('sub-judul','Tenaga Pendidik')
@section('halaman-sekarang','Tenaga Kependidikan')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('tpendidik.create') }}" class="btn btn-primary btn-flat btn-sm"><i class="fas fa-plus"></i> Tambah</a>
        </div>
      </div>

      <div class="card-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th width="10%">No</th>
              <th>Nama Pengelola tpendidik</th>
              <th>Foto</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach( $tpendidik as $result => $hasil)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $hasil->name }}</td>
              <td><img src="{{ asset( $hasil->foto ) }}" class="img-fluid" width="100px" alt=""></td>
              <td>
                <form action="{{ route('tpendidik.destroy', $hasil->id )}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="{{ route('tpendidik.edit', $hasil->id ) }}" class="btn btn-primary"><i class="fas fa-edit"></i>Edit</a>
                  <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i>Delete</button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
