@extends('backend.index')
@section('sub-judul','Tag')
@section('halaman-sekarang','Tag')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('tag.create') }}" class="btn btn-primary btn-flat btn-sm"><i class="fas fa-plus"></i> Tambah</a>
        </div>
      </div>

      <div class="card-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th width="10%">No</th>
              <th>Nama Tag</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach( $tag as $result => $hasil)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $hasil->name}}</td>
              <td>
                <form action="{{ route('tag.destroy', $hasil->id )}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="{{ route('tag.edit', $hasil->id ) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                  <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


@endsection
