@extends('backend.index')
@section('sub-judul','Tag')
@section('halaman-sekarang','Tag')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('tag.index') }}" class="btn btn-warning btn-sm">Kembali</a>
        </div>
      </div>
      <div class="card-body">

        <div class="row justify-content-center">
          <div class="col-md-6">
            <form class="form-horizontal" method="POST" action="{{ route('tag.store') }}">
              @csrf

              <div class="form-group">
                <label>Nama Tag</label>
                <input type="text" class="form-control" name="name" placeholder="Input Nama">
                <div class="text-danger">@error('name') {{ $message }} @enderror</div>
              </div>
              <button type="submit" class="btn btn-info">Simpan</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

@endsection
