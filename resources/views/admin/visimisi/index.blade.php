@extends('backend.index')
@section('sub-judul','Visi & Misi')
@section('halaman-sekarang','Visi & Misi')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('visimisi.create') }}" class="btn btn-primary btn-flat btn-sm"><i class="fas fa-plus"></i> Tambah</a>
        </div>
      </div>

      <div class="card-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th width="10%">No</th>
              <th>Visi</th>
              <th>Misi</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach( $visimisi as $result => $hasil)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $hasil->visi }}</td>
              <td>{{ $hasil->misi}}</td>
              <td>
                <form action="{{ route('visimisi.destroy', $hasil->id )}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="{{ route('visimisi.edit', $hasil->id ) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                  <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('sweetalert::alert')







@endsection
