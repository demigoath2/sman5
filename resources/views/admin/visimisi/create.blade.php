@extends('backend.index')
@section('sub-judul','Tambah Mata Pelajaran')
@section('halaman-sekarang','Tambah Mata Pelajaran')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('visimisi.index') }}" class="btn btn-warning btn-sm">Kembali</a>
        </div>
      </div>
      <div class="card-body">

        <div class="row justify-content-center">
          <div class="col-md-6">
            <form class="form-horizontal" method="POST" action="{{ route('visimisi.store') }}">
              @csrf

              <div class="form-group">
                <label>Visi</label>
                <textarea class="form-control" name="visi"></textarea>
                <div class="text-danger">@error('visi') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Misi</label>
                <textarea class="form-control" name="misi"></textarea>
                <div class="text-danger">@error('misi') {{ $message }} @enderror</div>
              </div>
              <button type="submit" class="btn btn-info">Simpan</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<script >
    CKEDITOR.replace('visi');
  </script>

<script >
    CKEDITOR.replace('misi');
  </script>

@endsection




