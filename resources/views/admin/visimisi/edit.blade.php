@extends('backend.index')
@section('sub-judul','Edit Mata Pelajaran')
@section('halaman-sekarang','Edit Mata Pelajaran')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('visimisi.index') }}" class="btn btn-warning btn-sm">Kembali</a>
        </div>
      </div>
      <div class="card-body">

        <div class="row justify-content-center">
          <div class="col-md-6">
            <form class="form-horizontal" method="POST" action="{{ route('visimisi.update', $visimisi->id) }}">
              @csrf
              @method('PUT')

              <div class="form-group">
                <label>Visi</label>
                <textarea class="form-control" name="visi">{{ $visimisi->visi }}</textarea>
                <div class="text-danger">@error('visi') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Misi</label>
                <textarea class="form-control" name="misi">{{ $visimisi->misi }}</textarea>
                <div class="text-danger">@error('misi') {{ $message }} @enderror</div>
              </div>

              <button type="submit" class="btn btn-info">Simpan</button>
            </form>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>


<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script >
  CKEDITOR.replace( 'visi' );
  CKEDITOR.replace( 'misi' );

</script>


@endsection
