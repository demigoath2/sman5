@extends('backend.index')
@section('sub-judul','Edit Guru')
@section('halaman-sekarang','Edit Guru')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="float-right">
          <a href="{{ route('guru.index') }}" class="btn btn-warning btn-sm">Kembali</a>
        </div>
      </div>
      <div class="card-body">

        <div class="row justify-content-center">
          <div class="col-md-6">
            <form class="form-horizontal" method="POST" action="{{ route('guru.update', $guru->id) }}" enctype="multipart/form-data">
              @csrf
              @method('PUT')

              <div class="form-group">
                <label>Nama Guru</label>
                <input type="text" class="form-control" name="name" value="{{ $guru->name }}">
                <div class="text-danger">@error('name') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Mata Pelajaran</label>
                <select class="form-control" name="mapelid">
                  <option value="" holder>Pilih Mata Pelajaran</option>
                  @foreach ($mapel as $result)
                  <option value="{{$result->id }}" {{ $result->id == $guru->mapelid ? 'selected' : '' }}>{{ $result->name}}</option>
                  @endforeach
                </select>
                <div class="text-danger">@error('mapelid') {{ $message }} @enderror</div>
              </div>
              <div class="form-group">
                <label>Foto Guru</label>
                <input type="file" class="form-control" id="thumbnail" name="foto" accept=".jpg, .png, .jpeg">
                <div class="text-danger">@error('foto') {{ $message }} @enderror</div>
              </div>
              <div id="thumbnail-preview"></div>
              <button type="submit" class="btn btn-info">Simpan</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){

    $("#thumbnail").on('change', function(){
      thumbnail(this);
    });
  })
  function thumbnail(input) {
    if (input.files && input.files[0]) {
      var thumnail_preview = $('#thumbnail-preview');
      thumnail_preview.empty();
      var reader = new FileReader();

      reader.onload = function(e) {
        $("<img />", {
          "src": e.target.result,
          "class": "preview-img pb-3",
          "width": "300",
          "height": "300"
        }).appendTo(thumnail_preview);
      }
      thumnail_preview.show();
      reader.readAsDataURL(input.files[0]);
    }
  }
</script>

@endsection
