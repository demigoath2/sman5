<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $fillable = ['name', 'mapelid', 'foto'];
    protected $table = 'table_guru';

    public function mapel()
    {
        return $this->belongsTo('App\Mapel', 'mapelid');
    }
}
