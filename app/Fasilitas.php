<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fasilitas extends Model
{
    protected $fillable = ['name', 'foto', 'content'];
    protected $table = 'table_fasilitas';
}
