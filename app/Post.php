<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['judul', 'category_id', 'content', 'foto', 'slug', 'users_id'];
    protected $table = 'table_post';

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function tag()
    {
        return $this->belongsToMany('App\Tags');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
