<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tpendidik extends Model
{
    protected $table = 'table_tpendidik';
    protected $fillable = ['name', 'foto'];
}
