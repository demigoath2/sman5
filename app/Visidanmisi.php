<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visidanmisi extends Model
{
    protected $table = "visimisi";
    protected $fillabale = ['visi','misi'];
}
