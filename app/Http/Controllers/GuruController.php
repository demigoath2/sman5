<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Guru, Mapel};

class GuruController extends Controller{
    public function index(){
        $guru = Guru::all();
        return view('admin.guru.index', compact('guru'));
    }

    public function create(){
        $mapel = Mapel::all();
        return view('admin.guru.create', compact('mapel'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|min:5|max:30',
            'foto' => 'required',
            'mapelid' => 'required'
        ]);

        $foto = $request->foto;
        $new_foto = time() . $foto->getClientOriginalName();

        Guru::create([
            'name' => $request->name,
            'mapelid' => $request->mapelid,
            'foto' => 'public/uploads/guru/' . $new_foto,


        ]);

        $foto->move('public/uploads/guru/', $new_foto);


        return redirect()->route('guru.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id){
        $mapel = Mapel::all();
        $guru = Guru::findorfail($id);
        return view('admin.guru.edit', compact('guru', 'mapel'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required|min:5|max:30',
            'mapelid' => 'required'

        ]);

        $guru = Guru::findorfail($id);

        if ($request->has('foto')) {
            $foto = $request->foto;
            $new_foto = time() . $foto->getClientOriginalName();
            $foto->move('public/uploads/guru/', $new_foto);

            $guru_data = [
                'name' => $request->name,
                'mapelid' => $request->mapelid,
            ];
        } else {
            $guru_data = [
                'name' => $request->name,
                'mapelid' => $request->mapelid,
            ];
        }

        $guru->update($guru_data);
        return redirect()->route('guru.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function destroy($id){
        $guru = Guru::findorfail($id);
        $guru->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
