<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\{Post, Tags, Category};

class PostController extends Controller{
    public function index(){
        $post = Post::all();
        return view('admin.post.index', compact('post'));
    }

    public function create(){
        $tags = Tags::all();
        $category = Category::all();
        return view('admin.post.create', compact('category', 'tags'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'judul' => 'required|min:3',
            'category_id' => 'required',
            'foto' => 'required',
            'content' => 'required'
        ]);
        $foto = $request->foto;
        $new_foto = time() . $foto->getClientOriginalName();

        $post = Post::create([
            'judul' => $request->judul,
            'category_id' => $request->category_id,
            'foto' => 'public/uploads/post/' . $new_foto,
            'content' => $request->content,
            'slug' => Str::slug($request->judul),
            'users_id' => Auth::id()


        ]);

        $post->tag()->attach($request->tag);

        $foto->move('public/uploads/post/', $new_foto);

        return redirect()->route('post.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id){
        $category = Category::all();
        $tags = Tags::all();
        $post = Post::findorfail($id);

        return view('admin.post.edit', compact('post', 'category', 'tags'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'judul' => 'required',
            'category_id' => 'required',
            'content' => 'required'
        ]);

        $post = Post::findorfail($id);

        if ($request->has('foto')) {
            $foto = $request->foto;
            $new_foto = time() . $foto->getClientOriginalName();
            $foto->move('public/uploads/posts/', $new_foto);

            $post_data = [
                'judul' => $request->judul,
                'category_id' =>  $request->category_id,
                'content' =>  $request->content,
                'foto' => 'public/uploads/posts/' . $new_foto,
                'slug' => Str::slug($request->judul)
            ];
        } else {
            $post_data = [
                'judul' => $request->judul,
                'category_id' =>  $request->category_id,
                'content' =>  $request->content,
                'slug' => Str::slug($request->judul)
            ];
        }


        $post->tag()->sync($request->tag);
        $post->update($post_data);


        return redirect()->route('post.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function destroy($id){
        $post = Post::findorfail($id);
        $post->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
