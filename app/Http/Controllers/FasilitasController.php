<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Fasilitas, Guru};

class FasilitasController extends Controller{
    public function index(){
        $fasilitas = Fasilitas::all();
        return view('admin.fasilitas.index', compact('fasilitas'));
    }

    public function create(){
        return view('admin.fasilitas.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|min:5|max:30',
            'foto' => 'required',
            'content' => 'required'
        ]);

        $foto = $request->foto;
        $new_foto = time() . $foto->getClientOriginalName();

        Fasilitas::create([
            'name' => $request->name,
            'foto' => 'public/uploads/fasilitas/' . $new_foto,
            'content' => $request->content
        ]);
        $foto->move('public/uploads/fasilitas/', $new_foto);


        return redirect()->route('fasilitas.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id){
        $fasilitas = Fasilitas::findorfail($id);
        return view('admin.fasilitas.edit', compact('fasilitas'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'foto' => 'required',
            'content' => 'required'
        ]);

        $fasilitas = Fasilitas::findorfail($id);

        if ($request->has('foto')) {
            $foto = $request->foto;
            $new_foto = time() . $foto->getClientOriginalName();
            $foto->move('public/updloads/fasilitas/', $new_foto);


            $fasilitas_data = [
                'name' => $request->name,
                'foto' => 'public/uploads/fasilitas/' . $new_foto,
                'content' => $request->content
            ];
        } else {
            $fasilitas_data = [
                'name' => $request->name,
                'content' => $request->content

            ];
        }
        $fasilitas->update($fasilitas_data);
        return redirect()->route('fasilitas.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function destroy($id){
        $fasilitas = Fasilitas::findorfail($id);
        $fasilitas->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
