<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Mapel, Guru};

class MapelController extends Controller{
    public function index(){
        $mapel = Mapel::all();
        return view('admin.mapel.index', compact('mapel'));
    }

    public function create(){
        return view('admin.mapel.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required'
        ]);

        Mapel::create([
            'name' => $request->name
        ]);

        return redirect('mapel')->withSuccess('Data Berhasil Disimpan');
    }

    public function edit($id){
        $mapel = Mapel::findorfail($id);
        return view('admin.mapel.edit', compact('mapel'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required'
        ]);

        $mapel_data = [
            'name' => $request->name
        ];

        Mapel::whereId($id)->update($mapel_data);
        return redirect()->route('mapel.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function destroy($id){
        $mapel = Mapel::findorfail($id);
        $mapel->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
