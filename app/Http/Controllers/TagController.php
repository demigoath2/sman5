<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tags;
use Illuminate\Support\Str;

class TagController extends Controller{
    public function index(){
        $tag = Tags::all();
        return view('admin.tag.index', compact('tag'));
    }

    public function create(){
        return view('admin.tag.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|min:3|max:30'
        ]);

        Tags::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name)
        ]);
        return redirect()->back()->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id){
        $tag = Tags::findorfail($id);
        return view('admin.tag.edit', compact('tag'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required|min:3|max:30'
        ]);


        $tag_data = [
            'name' => $request->name,
            'slug' => Str::slug($request->name)
        ];

        Tags::whereId($id)->update($tag_data);

        return redirect()->route('tag.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function destroy($id){
        $tags = Tags::findorfail($id);
        $tags->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
