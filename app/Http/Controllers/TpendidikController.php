<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tpendidik;

class TpendidikController extends Controller{
    public function index(){
        $tpendidik = Tpendidik::all();
        return view('admin.tpendidik.index', compact('tpendidik'));
    }

    public function create(){
        return view('admin.tpendidik.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|min:5|max:30',
            'foto' => 'required',
        ]);

        $foto = $request->foto;
        $new_foto = time() . $foto->getClientOriginalName();

        Tpendidik::create([
            'name' => $request->name,
            'foto' => 'public/uploads/tpendidik/' . $new_foto

        ]);

        $foto->move('public/uploads/tpendidik/', $new_foto);


        return redirect()->route('tpendidik.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id){
        $tpendidik = Tpendidik::findorfail($id);
        return view('admin.tpendidik.edit', compact('tpendidik'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required|min:5|max:30',
            'foto' => 'required'

        ]);

        $tpendidik = Tpendidik::findorfail($id);

        if ($request->has('foto')) {
            $foto = $request->foto;
            $new_foto = time() . $foto->getClientOriginalName();
            $foto->move('public/uploads/tpendidik/', $new_foto);

            $tpendidik_data = [
                'name' => $request->name,
                'foto' => $request->foto,
            ];
        } else {
            $tpendidik_data = [
                'name' => $request->name,
                'foto' => $request->foto,
            ];
        }

        $tpendidik->update($tpendidik_data);
        return redirect()->route('tpendidik.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function destroy($id){
        $tpendidik = Tpendidik::findorfail($id);
        $tpendidik->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
