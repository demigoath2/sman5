<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $fillable = ['name'];
    protected $table = 'table_mapel';


    public function guru()
    {
        return $this->hasOne('App\Guru');
    }
}
