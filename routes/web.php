<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
	return view('auth/login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/mapel', 'MapelController');
    Route::resource('/guru', 'GuruController');
    Route::resource('/fasilitas', 'FasilitasController');
    Route::resource('/tpendidik', 'TpendidikController');
    Route::resource('/category', 'CategoryController');
    Route::resource('/tag', 'TagController');
    Route::resource('/post', 'PostController');
    Route::resource('/visimisi','VisiMisiController');
});
